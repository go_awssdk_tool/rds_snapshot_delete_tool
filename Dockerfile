#############################################
# builder
#############################################
FROM golang:1.19.1-alpine3.16 AS builder

WORKDIR /src

RUN \
  echo http://dl-cdn.alpinelinux.org/alpine/edge/community/ >> /etc/apk/repositories && \
  apk update --no-cache && \
  apk add --virtual .build \
  git \
  gcc \
  make \
  curl \
  tzdata \
  musl-dev

COPY ./go.mod /src
COPY ./go.sum /src

RUN go mod tidy

ENV CGO_ENABLED 0
ENV GOOS linux
ENV GOARCH amd64


RUN go build -o /bin -ldflags '-s -w' /src/main.go


#############################################
# runner
#############################################
FROM alpine:latest as runner

WORKDIR /app

COPY --from=builder /bin/main /app/main
COPY input/ /input

RUN apk update --no-cache && \
    apk --no-cache add ca-certificates tzdata && \
    update-ca-certificates && \
    cp /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

CMD ["/app/main"]