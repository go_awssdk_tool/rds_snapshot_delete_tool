# rds_snapshot_delete_tool



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!


## ローカルでの動作方法


(1)jsonファイルに削除対象と除外対象をリストに追記
delete_rds_snap_list.json


(2)削除対象確認
```
$ go run main.go
```

(3)

## dryrun 実施時の挙動

delete_rds_snap_list.json
```
{
	"delete_target": [
		"companyinfo-final-snapshot"
	],
	"exclude_target": [
		""
	]
}
```



以下のような結果が返ってきます

```
$ go run main.go

**Snapshot list before deletion**
*----------------------------------------------------*
companyinfo-final-snapshot
kpi-test-final-snapshot
*-----------------------------------------------------------*

**Deletion Target**
*-----------------------------------------------------------*
companyinfo-final-snapshot
*-----------------------------------------------------------*

```

## execute 実施時の挙動

delete_rds_snap_list.json
```
{
	"delete_target": [
		"companyinfo-final-snapshot"
	],
	"exclude_target": [
		""
	]
}
```



以下のような結果が返ってきます

```
$ go run main.go -execute

**Snapshot list before deletion**
*----------------------------------------------------*
companyinfo-final-snapshot
kpi-test-final-snapshot
*-----------------------------------------------------------*

**Deletion Target**
*-----------------------------------------------------------*
companyinfo-final-snapshot
*-----------------------------------------------------------*
 

**Deletion start**
*-----------------------------------------------------------*
{
  DBSnapshot: {
    AllocatedStorage: 20,
    AvailabilityZone: "ap-northeast-1a",
    DBInstanceIdentifier: "companyinfo",
    DBSnapshotArn: "arn:aws:rds:ap-northeast-1:xxxxxxxxxxxx:snapshot:companyinfo-final-snapshot",
    DBSnapshotIdentifier: "companyinfo-final-snapshot",
    DbiResourceId: "xxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    Encrypted: true,
    Engine: "mysql",
    EngineVersion: "5.7.33",
    IAMDatabaseAuthenticationEnabled: false,
    InstanceCreateTime: 2019-10-24 02:34:39.344 +0000 UTC,
    KmsKeyId: "arn:aws:kms:ap-northeast-1:xxxxxxxxxxxx:key/xxxxxxxxxxxxxxxxxxxx",
    LicenseModel: "general-public-license",
    MasterUsername: "admin",
    OptionGroupName: "default:mysql-5-7",
    OriginalSnapshotCreateTime: 2022-07-14 05:39:36.921 +0000 UTC,
    PercentProgress: 100,
    Port: 3306,
    SnapshotCreateTime: 2022-07-14 05:39:36.921 +0000 UTC,
    SnapshotTarget: "region",
    SnapshotType: "manual",
    Status: "deleted",
    StorageType: "gp2",
    VpcId: "vpc-xxxxxx"
  }
}
*------------------------------------------------------*

**List of Snapshots after deletion**
*-----------------------------------------------------------*
kpi-test-final-snapshot
*------------------------------------------------------*


```