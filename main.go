package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/rds"
)

type jsonList struct {
	DeleteTarget  []string `json:"delete_target"`
	ExcludeTarget []string `json:"exclude_target"`
}

const decorativeFont string = "*-----------------------------------------------------------*"

func main() {

	//executeフラグ処理
	var execute = flag.Bool("execute", false, "")
	flag.Parse()

	//Jsonファイルの読み込み処理ブロック
	var targetList jsonList
	deleteRdsSnapList, err := os.ReadFile("./input/delete_rds_snap_list.json")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	if err := json.Unmarshal([]byte(deleteRdsSnapList), &targetList); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//ここでdeletion_targetに値がない場合、処理終了
	if len(targetList.DeleteTarget) == 0 {
		fmt.Println("No value for deletion_target")
		os.Exit(1)
	}

	//AWSセッション処理
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			Region: aws.String("ap-northeast-1"),
		},
	}))

	//削除前のRDSスナップショット一覧取得
	result, err := getInstanceSnapShots(sess)
	if err != nil {
		fmt.Println("Got an error retrieving instance snapshots:")
		fmt.Println(err)
		os.Exit(1)
	}
	if len(result.DBSnapshots) < 1 {
		fmt.Println("Could not find any instance snapshots")
		os.Exit(1)
	}

	//削除前のRDSスナップショット一覧出力
	beforMessage := "Snapshot list before deletion"
	snapShotsListAcquisition(beforMessage, result)

	//除外リストに入っているスナップショット名を除いた削除リストを作成
	tmpDeleteList := createTmpDeleteList(targetList.ExcludeTarget, result)
	if len(tmpDeleteList) == 0 {
		fmt.Printf("There is no target for deletion.")
		os.Exit(1)
	}

	//除外リストの除外された残りのスナップショット一覧から該当するスナップショットを確認する処理
	deleteList := createDeleteList(targetList.DeleteTarget, tmpDeleteList)

	//executeのフラグが付いていた場合のみ削除実施
	if *execute {
		//削除対象がない場合は、削除処理を実施しない
		if len(deleteList) == 0 {
			fmt.Println("No objects to delete, process terminated")
		} else {
			// RDSスナップショット削除処理
			err := deleteInstanceSnapShots(sess, deleteList)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			//削除後のRDSスナップショット一覧取得
			result, err := getInstanceSnapShots(sess)
			if err != nil {
				fmt.Println("Got an error retrieving instance snapshots:")
				fmt.Println(err)
				os.Exit(1)
			}

			if len(result.DBSnapshots) < 1 {
				fmt.Println("Could not find any instance snapshots")
				os.Exit(1)
			}

			//削除後のRDSスナップショット一覧出力
			afterMessage := "List of Snapshots after deletion"
			snapShotsListAcquisition(afterMessage, result)
		}
	}
}

func getInstanceSnapShots(sess *session.Session) (*rds.DescribeDBSnapshotsOutput, error) {
	svc := rds.New(sess)

	req := &rds.DescribeDBSnapshotsInput{
		SnapshotType: aws.String("manual"),
	}

	result, err := svc.DescribeDBSnapshots(req)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func deleteInstanceSnapShots(sess *session.Session, deleteList []string) error {
	svc := rds.New(sess)

	fmt.Println(decorativeFont)
	fmt.Println(len(deleteList))
	fmt.Println(decorativeFont)

	fmt.Println("\n**Deletion start**\n" + decorativeFont)
	for _, v := range deleteList {
		res, err := svc.DeleteDBSnapshot(&rds.DeleteDBSnapshotInput{
			DBSnapshotIdentifier: aws.String(v),
		})
		if err != nil {
			return err
		}
		fmt.Println(res)
	}
	fmt.Println("\n**Deletion Target**\n" + decorativeFont)

	return nil
}

func snapShotsListAcquisition(message string, snapshotlist *rds.DescribeDBSnapshotsOutput) {

	fmt.Println("\n**" + message + "**\n" + decorativeFont)
	for _, v := range snapshotlist.DBSnapshots {
		fmt.Println(*v.DBSnapshotIdentifier)
	}
	fmt.Println(decorativeFont)

}

func createTmpDeleteList(excludeTarget []string, snapshotlist *rds.DescribeDBSnapshotsOutput) []string {

	counter := map[string]int{}
	tmpDeleteList := []string{}

	for _, v := range snapshotlist.DBSnapshots {
		counter[*v.DBSnapshotIdentifier]++
	}
	for _, v := range excludeTarget {
		counter[v]++
	}

	for k, v := range counter {
		if v == 1 {
			tmpDeleteList = append(tmpDeleteList, k)
		}
	}

	return tmpDeleteList
}

func createDeleteList(deleteTarget []string, tmpDeleteList []string) []string {
	deleteList := []string{}

	for _, v := range tmpDeleteList {
		for _, dt := range deleteTarget {
			if strings.Contains(v, dt) {
				deleteList = append(deleteList, v)
			}
		}
	}

	fmt.Println("\n**Deletion Target**\n" + decorativeFont)
	for _, d := range deleteList {
		fmt.Println(d)
	}
	fmt.Println(decorativeFont + "\n ")

	return deleteList
}
